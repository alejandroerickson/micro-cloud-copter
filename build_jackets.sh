#!/bin/sh

SCALE="1.02"
RESOLUTION="100"
MOTOR_SIZES="7 8.5"
JACKET="0.9"
MAXMOTOR="8.5"
MOTOR_HEIGHT="20.6"
PROP_HOLE="2.5"
JACKET_TOP="0.9"

for PARAM_MOTOR in ${MOTOR_SIZES}
do
    OpenSCAD -o Jacket_Motor${PARAM_MOTOR}_Thickness${JACKET}_MaxMotor${MAXMOTOR}_Scaling${SCALE}_Height${MOTOR_HEIGHT}_Prop${PROP_HOLE}_Jtop${JACKET_TOP}.stl -D \
"\
Motor_Diameter=${PARAM_MOTOR};\
Motor_Height=${MOTOR_HEIGHT};\
Maximum_Motor_Diameter=${MAXMOTOR};\
Minimum_Jacket_Thickness=${JACKET};\
Motor_Hole_Scaling=${SCALE};\
Motor_Jacket_Top_Thickness=${JACKET_TOP};
Motor_Shaft_Hole_Diameter=${PROP_HOLE};
Resolution=${RESOLUTION};\
Which_Parts=\"jacket\";\
"\
 Dream\ Bezier.scad &
#> /dev/null
done

