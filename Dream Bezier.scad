/************************************************
* 
* Title: Micro Cloud Copter Frame
* Author: Alejandro Erickson
*
* Source code: https://bitbucket.org/alejandroerickson/micro-cloud-copter
*
* Version: v0.0b
*
* About: The Micro Cloud Copter Frame is a customizable brushed micro
multicopter frame inspired by Benedikt Haak's dream catcher quadcopter
frame. It has a motor pop-off feature that protects the quad during
impacts, and the floating Flight Deck which dampens
vibrations. Several valuable improvements are implemented:
*
* Each motor is fixed to a casing which protects the leads, reduces
motor twisting, and secures motor end-caps during crashes.
*
* The Micro Cloud Copter can be customised to suit any reasonable
number of motors, any multicopter configuration, and any size of
brushed motors.
*
* The Micro Cloud Copter is implemented in OpenScad for easy
customisability.
*
*
* Attribution credits: Chad Kirby, WilliamAAdams (Thingiverse user)
*
* Copyright (c) 2015 Alejandro Erickson, released under and subject to
* the license: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
* http://creativecommons.org/licenses/by-sa/4.0/
*
**************************************************/

include <BezierScad.scad>

/* [Quadcopter Dimensions] */

//Choose your configuration.  For "User_Configuration" you must set
//User_Motors. See examples in source code.
Configuration="quadx"; //["user":User_Configuration, "quadplus":Quad_Plus, "quadx":Quad_X, "octoplus":Octo_Plus, "octov":Octo_V, "hexax":Hexa_X, "hexai":Hexa_I, "triy":Tri_Y, "triiy":Tri_IY]

//User_Motors = [[radius*cos(3*45),radius*sin(3*45)],
//	  [radius*cos(1*45),radius*sin(1*45)],
//	  [radius*cos(-1*45),radius*sin(-1*45)],
//	  [radius*cos(-3*45),radius*sin(-3*45)]];
//N_Motors=4;

//Distance from the centre of the quad to the middle of motors in
//precomputed configurations
Radius=sqrt(2)*40;

Flight_Controller_Diameter=55;

//Minimum width of frame struts
Strut_Width=1.5;

//The thickness of your quadcopter frame determines how strong it is, how flexible it is, how much it dampens vibration, etc.
Frame_Thickness=3*2;

Rounded_Corners=false;

/* [Motor Mount Points] */

//Maximimum diameter of motor that the frame will accommodate
Maximum_Motor_Diameter=8.5;
//Thickness of jacket for maximum size motor
Minimum_Jacket_Thickness=.9;

//This adds a slight bevel inside the motor cap so that you don't have to cut away any material to insert motors.
Motor_Cap_Inner_Bevel=0.6;

//The size of the side opening on the motor caps
Motor_Cap_Side_Opening=Maximum_Motor_Diameter+2*Minimum_Jacket_Thickness-1;

/* [Motor Enclosure] */

//Actual motor diameter (only affects Motor Jackets)
Motor_Diameter= 8.5;// [6,7,8.5]

//Height of motor (only affects Motor Jackets)
Motor_Height=20.6;

//This is the thickness of the bit covering the top of the motor.  It
//goes between the motor and propeller, so it is dependent on how much
//of a gap you have there.  Try to fill the gap to better protect your
//motors, but don't let the propeller touch the Motor Jacket.
Motor_Jacket_Top_Thickness=.9;

//The part that goes over the motor covers two arcs around the motor.
//This is the angle subtended by those arcs.
Motor_Jacket_Wedge_Angle=90;

//This is how far the jacket extends below the motor.  You certainly
//want enough to protect the wire, but it can also double as landing
//gear if it extends beyond your battery and stuff.
Motor_Jacket_Bottom_Thickness=6;

//This is the diameter of the hole in the Motor Jacket for the motor's
//propeller shaft to come through.  8.5mm motors have a larger bushing
//and this value should probably be at least 2.5 to accomodate that.
Motor_Shaft_Hole_Diameter=2.5;


//Motor hole scaling.  The frame is built with
//scaled_motor_diameter=Motor_Hole_Scaling*Motor_Diameter, so 1.0 is
//the no-change default.
Motor_Hole_Scaling=1.02;


/* [Parts and Quality Options] */

//Which parts do you want to show
Which_Parts="double"; // ["frame":Frame, "jacket":Motor_Jacket, "double":Double_Jacket]

Resolution=20; //[20,50,100]

/* [Hidden] */

//The thickness of the motor cap wall
Motor_Cap_Wall_Thickness=Strut_Width;

/* UNIMPLEMENTED PARAMETERS */

//depracated
//This is the thickness of the part that goes over the motor.  This
//modifies the Motor Foot, Motor Jacket, and the Whole Frame.
//Motor_Jacket_Thickness=0.9+.7; // [0.3:2]


//List motor tilts away from z (vertical) axis (in the direction specified in Motor_Angles).
//Motor_Tilts=[-10,-10,-15,0,0,-15];
//Motor_Tilts=[45,30,10,0];

//(ANGLES UNIMPLEMENTED) List of angles motor will tilt toward (in the (x,y), horizontal, plane).  By convention, 0,90,180,270 are right,forward, left, back, respectively for up-motors, and directions are reversed for down-motors
//Motor_Angles=[90,90,90,90];
//Motor_Angles=[90,90,90,90,90,90,90,90];

//Actual motor length;
//Motor_Length=20;//[20]



//calculated variables
//Flight Controller Width
FC_Width=Flight_Controller_Diameter;

//Flight Controller Length
FC_Length=Flight_Controller_Diameter;

function quadx(radius=Radius) = [[radius*cos(3*45),radius*sin(3*45)],
				 [radius*cos(1*45),radius*sin(1*45)],
				 [radius*cos(-1*45),radius*sin(-1*45)],
				 [radius*cos(-3*45),radius*sin(-3*45)]];

function quadplus(radius=Radius) =  [[radius*cos(2*45),radius*sin(2*45)],
				 [radius*cos(0*45),radius*sin(0*45)],
				 [radius*cos(-2*45),radius*sin(-2*45)],
				 [radius*cos(-4*45),radius*sin(-4*45)]];

function octoplus(radius=Radius) = [[-40,40],
				 [0,40*sqrt(2)],
				 [40,40],
				 [40*sqrt(2),0],
				 [40,-40],
				 [0,-40*sqrt(2)],
				 [-40,-40],
				    [-40*sqrt(2),0]];

function octov(radius=Radius) = [[radius*cos(3*45+45/2),radius*sin(3*45+45/2)],
				 [radius*cos(2*45+45/2),radius*sin(2*45+45/2)],	
				 [radius*cos(1*45+45/2),radius*sin(1*45+45/2)],
				 [radius*cos(0*45+45/2),radius*sin(0*45+45/2)],
				 [radius*cos(-1*45+45/2),radius*sin(-1*45+45/2)],
				 [radius*cos(-2*45+45/2),radius*sin(-2*45+45/2)],
				 [radius*cos(-3*45+45/2),radius*sin(-3*45+45/2)],	
				 [radius*cos(-4*45+45/2),radius*sin(-4*45+45/2)]];





function hexax(radius=Radius) = [[radius*cos(4*30),radius*sin(4*30)],
				 [radius*cos(2*30),radius*sin(2*30)],
				 [radius*cos(0*30),radius*sin(0*30)],
				 [radius*cos(-2*30),radius*sin(-2*30)],
				 [radius*cos(-4*30),radius*sin(-4*30)],
				 [radius*cos(6*30),radius*sin(6*30)]];


function hexai(radius=Radius) =  [[radius*cos(5*30),radius*sin(5*30)],
				  [radius*cos(3*30),radius*sin(3*30)],
				 [radius*cos(1*30),radius*sin(1*30)],
				 [radius*cos(-1*30),radius*sin(-1*30)],
				  [radius*cos(-3*30),radius*sin(-3*30)],
				  [radius*cos(-5*30),radius*sin(-5*30)]];

function triiy(radius=Radius) = [[radius*cos(5*30),radius*sin(5*30)],
				 [radius*cos(1*30),radius*sin(1*30)],
				  [radius*cos(-3*30),radius*sin(-3*30)]];


function triy(radius=Radius) =  [[radius*cos(3*30),radius*sin(3*30)],
				 [radius*cos(-1*30),radius*sin(-1*30)],
				  [radius*cos(-5*30),radius*sin(-5*30)]];



Motors=(Configuration=="user")?
     User_Motors:
     ((Configuration=="quadplus")?
      quadplus():
      ((Configuration=="quadx")?
       quadx():
       ((Configuration=="octoplus")?
	octoplus():
	((Configuration=="hexax")?
	 hexax():
	 ((Configuration=="hexai")?
	  hexai():
	 ((Configuration=="triy")?
	  triy():
	 ((Configuration=="triiy")?
	  triiy():
	  ((Configuration=="octov")?
	   octov():0))))))));

N_Motors=
     ((Configuration=="user")?
      User_N_Motors:
      ((Configuration=="quadplus" || Configuration=="quadx")?
       4:
       ((Configuration=="hexax" || Configuration=="hexai")?
	6:
       ((Configuration=="triy" || Configuration=="triiy")?
	3:
	((Configuration=="octov" || Configuration == "octoplus")?
	 8:0)))));

//echo(Motors,N_Motors);

sphere_radius=1;
$fn=Resolution;

scaled_motor_diameter=Motor_Hole_Scaling*Motor_Diameter;
cap_wall_thickness=Motor_Cap_Wall_Thickness;
cap_wall_opening=Motor_Cap_Side_Opening;

module halfcircle(r) { 
     difference() { 
	  translate([0,0])circle(r); 
	  translate([-r,-.001])square([r*3,r*2]); 
     } 
} 

module sphere_alternate(r) { 
     hull() rotate_extrude() 
	  rotate([0,0,90])halfcircle(r); 
} 

module motor_mount_outer(
     inside_diameter=Motor_Hole_Scaling*Maximum_Motor_Diameter+2*Minimum_Jacket_Thickness,
     motor=0,
     cap_wall_thickness=cap_wall_thickness,
     height_factor=1)
{
     outer_radius=inside_diameter/2+cap_wall_thickness;
     outer_height=height_factor*Frame_Thickness;
//h+Frame_Thickness-sphere_radius;
     translate([Motors[motor][0],Motors[motor][1],0])
	  difference(){
	  translate([0,0,0])
     rotate([0,0,90])
	       translate([0,0,0])
	       cylinder(h=outer_height*2,r=outer_radius, center=true);
	  translate([0,0,-outer_height]) 
	       cube([outer_radius*10,
		     outer_radius*10,
		     outer_height*2],
		    center=true);
     }
}

module motor_mount_inner(
     h=Frame_Thickness,
     inside_diameter=Motor_Hole_Scaling*Maximum_Motor_Diameter+2*Minimum_Jacket_Thickness,
     motor=0,
     cap_wall_thickness=cap_wall_thickness,
     cap_wall_opening=cap_wall_opening,
     cap_wall_angle=90,
     height_factor=1)
{
     inner_radius=inside_diameter/2;
     outer_radius=inside_diameter/2+cap_wall_thickness;
     inner_height=2*height_factor*Frame_Thickness;
     bevel_thickness=Motor_Cap_Inner_Bevel;

     theta=atan2(Motors[motor][1],Motors[motor][0]);
    

	  
     translate([Motors[motor][0],
		Motors[motor][1],0])
	  rotate([0,0,theta+90])
	  union(){ 
	  cylinder(h=inner_height,r=inner_radius, center=true);
	  translate([0,0,bevel_thickness/2])
	       cylinder(
	  	    h=bevel_thickness,
	  	    r2=inner_radius,
	  	    r1=inner_radius+bevel_thickness,
	  	    center=true);

	       //rotate([0,0,cap_wall_angle/2])wedge(inner_height,outer_radius+.1,cap_wall_angle);
	  translate([0,outer_radius/2,0])
	       cube([cap_wall_opening,outer_radius,inner_height], center=true);
     }
}

/************
* strut(...) outputs a BezWall(...) between two motors.  To function
* properly, it requires that (xl,yl) and (xr,yr) be the coordinates
* consecutive (in the clockwise direction) motors, Motors[i] and
* Motors[i+1] (indices modulo N_Motors).
* 
* cluster_factor determines how tightly the non-terminus anchor points
* of the BezWall are clustered around the Flight Deck, and thus how
* "sharp" the BezWall is at its middle.
*
* width_factor determines how wide the strut is near its middle.  The
* width at the terminals is determined by Motor_Cap_Wall_Thickness (also Strut_Width).
*
* height_factor determines how high the strut is at its terminals.  The height at the middle is determined by Frame_Thickness
*
*************/
module strut(xl=-50,yl=40,
	     xr=70,yr=85,
	     hh=Frame_Thickness,
	     ww=Strut_Width,
	     cluster_factor=2.5,
	     width_factor=2,
	     height_factor=2,
	     mink=false,
	     sr=sphere_radius,
	     max_motor_diameter=Motor_Hole_Scaling*Maximum_Motor_Diameter,
	     min_jacket_thickness=Minimum_Jacket_Thickness,
	     cap_wall_thickness=cap_wall_thickness){
     mod_sw=(max_motor_diameter+2*min_jacket_thickness+cap_wall_thickness)/2;
     //thetal is the angle that (xl,yl) makes with the x-axis in the
     //range -180<= thetal <= 180 (see
     //https://en.wikipedia.org/wiki/Atan2).  Note that in OpenScad we
     //have atan2(-0,-1)=-180, whilst atan2(0,-1)=180.     
// TODO: deal with this special case, if necessary.
     thetal=atan2(yl,xl);
     //Next, we want the BezWall to terminate on a point tangent to a
     //circle that centered on the motor, but runs down the middle of
     //the motor wall.  See Figure Bezier Strut Anchor Point.
     distl2=yl*yl+xl*xl;
     delta_thetal=asin(mod_sw/sqrt(distl2));
     thetal1=thetal-delta_thetal;
     //(xl1,yl1) is the desired terminus of the BezWall at the first motor
     xl1=sqrt(distl2)*cos(thetal1);
     yl1=sqrt(distl2)*sin(thetal1);
     // The same computation for (yr,xr)
     thetar=atan2(yr,xr);
     distr2=yr*yr+xr*xr;
     delta_thetar=asin(mod_sw/sqrt(distr2));
     thetar1=thetar+delta_thetar;
     //(xr1,yr1) is the desired terminus of the BezWall at the second motor
     xr1=sqrt(distr2)*cos(thetar1);
     yr1=sqrt(distr2)*sin(thetar1);
     //find the place where we anchor this bezier curve to the Flight
     //Deck.  Note that a bit of case analysis would improve this, and
     //we could do better when abs(FC_Width-FC_Height) is large.  At
     //the moment we assume the Flight Deck is a regular polygon
     //(e.g., a square).
     yfc=min(FC_Width,FC_Length)/2;
     atanmin=min(atan2(yl,xl),atan2(yr,xr));
     atanmax=max(atan2(yl,xl),atan2(yr,xr));
     theta=(atanmax-atanmin>180)?(atanmin+atanmax)/2+180:(atanmin+atanmax)/2;
     apex=yfc*cos(theta);
     apex_y=yfc*sin(theta);
//     theta_gap=(atanmax-atanmin>180)?(atanmax-atanmin)-180:(atanmax-atanmin);
     theta_gap=(atanmax-atanmin>180)?atanmin-(atanmax-360):(atanmax-atanmin);
//     cluster_step=cluster_factor*theta_gap/(yfc);
     cluster_step=theta_gap/12;
     echo(xl,yl,xr,yr,atanmin,atanmax,theta_gap,cluster_step);
     w=(mink==true)?ww-2*sr:ww;
     h=(mink==true)?hh-2*sr:hh;
     middle_width=width_factor*(w);
     middle_height=(h)/height_factor;
     translate([0,0,(mink==true)?sr:0])
     BezWall(
	  height=h,
	  [
	       [xl1,yl1],
	       [yfc*cos(theta-cluster_step*(-3)),
		yfc*sin(theta-cluster_step*(-3))],	   
	       [yfc*cos(theta-cluster_step*(-2)),
		yfc*sin(theta-cluster_step*(-2))],	   
	       [yfc*cos(theta-cluster_step*(-1)),
		yfc*sin(theta-cluster_step*(-1))],	   
	       [yfc*cos(theta-cluster_step*1),
		yfc*sin(theta-cluster_step*1)],	   
	       [yfc*cos(theta-cluster_step*2),
		yfc*sin(theta-cluster_step*2)],	   
	       [yfc*cos(theta-cluster_step*3),
		yfc*sin(theta-cluster_step*3)],	   
	       [xr1,yr1]],
	  width=w,
	  heightCtls=[
	       h,
	       middle_height,
	       middle_height,
	       middle_height,
	       middle_height,
	       middle_height,
	       middle_height,
	       h],
	  widthCtls=[w,
		     middle_width,
		     middle_width,
		     middle_width,
		     middle_width,
		     middle_width,
		     middle_width,
		     w],
	  resolution=6,
	  centered=true,
	  showCtlR=0,
	  steps=Resolution);
}

module rounded_strut(xl=-50,yl=40,
		     xr=70,yr=85,
		     hh=Frame_Thickness,
		     ww=Strut_Width,
		     cluster_factor=2.5,
		     width_factor=1.7,
		     height_factor=1.7,
		     sr=sphere_radius){
     minkowski(){
	  sphere_alternate(sphere_radius);
	  strut(xl=xl,yl=yl,xr=xr,yr=yr,
		hh=hh,ww=ww,
		cluster_factor=cluster_factor,
		width_factor=width_factor,
		height_factor=height_factor,
		mink=true,
		sr=sr);
     }
}


/********************
MotorCase
The Motor Case is designed to protect motors and wires.
*********************/

module MotorJacket(     
     m_diameter=Motor_Diameter,
     motor_height=Motor_Height,
     s_diameter=scaled_motor_diameter,
     thickness=
     Motor_Hole_Scaling
     *(Maximum_Motor_Diameter-Motor_Diameter)/2
     +Minimum_Jacket_Thickness,
     top_thickness=Motor_Jacket_Top_Thickness,
     bottom_thickness=Motor_Jacket_Bottom_Thickness,
     angle=Motor_Jacket_Wedge_Angle,
     hole_diameter=Motor_Shaft_Hole_Diameter,
     wire_feed_inside=1.5){
     outside_height=motor_height+top_thickness+bottom_thickness;
     outside_radius=s_diameter/2+thickness;
     inside_radius=s_diameter/2;
     translate([-outside_height/2,0,(inside_radius)*sin(angle/2)])rotate([0,90,0])
     difference(){
	  union(){
	  cylinder(h=outside_height,r=outside_radius);
	  translate([0,0,outside_height-1])cylinder(h=2,r=outside_radius+1);
	  translate([0,0,5*motor_height/6])cylinder(h=2,r=outside_radius+1);
	  }
	  translate([0,0,top_thickness])cylinder(h=motor_height,r=inside_radius);
	  translate([2*inside_radius+(inside_radius)*sin(angle/2),0,0])
	       cube([4*inside_radius,4*outside_radius,
		     outside_height*3],center=true);
	  translate([-2*inside_radius-(inside_radius)*sin(angle/2),0,0])
	       cube([4*inside_radius,4*outside_radius,
		     outside_height*3],center=true);
//hole for wire feed
	  translate([0,0,outside_height+top_thickness]) cube([outside_radius*4,wire_feed_inside,outside_height*2],center=true);
		    cylinder(h=top_thickness,r=hole_diameter/2);
     }
}


module DoubleJacket(     
     m_diameter=Motor_Diameter,
     motor_height=Motor_Height,
     s_diameter=scaled_motor_diameter,
     thickness=
     Motor_Hole_Scaling
     *(Maximum_Motor_Diameter-Motor_Diameter)/2
     +Minimum_Jacket_Thickness,
     top_thickness=Motor_Jacket_Top_Thickness,
     bottom_thickness=Motor_Jacket_Bottom_Thickness,
     angle=Motor_Jacket_Wedge_Angle,
     hole_diameter=Motor_Shaft_Hole_Diameter,
     wire_feed_inside=1.5){
     outside_height=2*motor_height+2*top_thickness+bottom_thickness;
     outside_radius=s_diameter/2+thickness;
     inside_radius=s_diameter/2;
     translate([-outside_height/2,0,(inside_radius)*sin(angle/2)])rotate([0,90,0])
     difference(){
	  union(){
	  cylinder(h=outside_height,r=outside_radius);
	  translate([0,0,motor_height])cylinder(h=2,r=outside_radius+1);
	  }
	  translate([0,0,top_thickness])cylinder(h=motor_height,r=inside_radius);
	  translate([0,0,motor_height+bottom_thickness+top_thickness])cylinder(h=motor_height,r=inside_radius);
	  translate([2*inside_radius+(inside_radius)*sin(angle/2),0,0])
	       cube([4*inside_radius,4*outside_radius,
		     outside_height*3],center=true);
	  translate([-2*inside_radius-(inside_radius)*sin(angle/2),0,0])
	       cube([4*inside_radius,4*outside_radius,
		     outside_height*3],center=true);
//hole for wire feed
	  translate([0,0,motor_height+top_thickness+bottom_thickness/2]) cube([outside_radius*4,wire_feed_inside,2*bottom_thickness],center=true);
		    cylinder(h=top_thickness,r=hole_diameter/2);
		    translate([0,0,outside_height-top_thickness]) cylinder(h=top_thickness,r=hole_diameter/2);
     }
}
if(Which_Parts=="frame"){
     union(){
	  difference(){
	       for(m=[0:N_Motors-1]){
		    union(){
			 motor_mount_outer(motor=m);
			 if(Rounded_Corners){
			      rounded_strut(xl=Motors[m][0],
					    yl=Motors[m][1],
					    xr=Motors[(m+1)%N_Motors][0],
					    yr=Motors[(m+1)%N_Motors][1],
					    sr=sphere_radius);
			 }else{
			      strut(xl=Motors[m][0],
				    yl=Motors[m][1],
				    xr=Motors[(m+1)%N_Motors][0],
				    yr=Motors[(m+1)%N_Motors][1],
				    mink=false,
				    sr=sphere_radius);

			 }
		    }
	       }
		    for(m=[0:N_Motors-1]){
			 motor_mount_inner(motor=m);
		    }
	       }
	  }
}else if(Which_Parts=="jacket"){
     MotorJacket();
}else if(Which_Parts=="double"){
     DoubleJacket();
}



