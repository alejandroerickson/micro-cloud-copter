#!/bin/sh

CONFIGURATIONS="octoplus"
#"quadx octoplus hexax"
ROUNDED="false"
SCALE="1.02"
RESOLUTION="100"
MAXMOTOR="8.5"
FRAME="70"
STRUT_WIDTH="3"
STRUT_HEIGHT="6";
JACKET="0.9"
RADIUS="sqrt(2)*40"

for PARAM_CONFIG in ${CONFIGURATIONS}
do
    OpenSCAD -o ${PARAM_CONFIG}_Scaling${SCALE}_Motors${MAXMOTOR}_Frame${FRAME}_Strut${STRUT_WIDTH}_Height${STRUT_HEIGHT}_Jacket${JACKET}.stl -D \
"\
Configuration=\"${PARAM_CONFIG}\";\
Flight_Controller_Diameter=${FRAME};\
Strut_Width=${STRUT_WIDTH};\
Radius=${RADIUS};\
Frame_Thickness=${STRUT_HEIGHT};\
Rounded_Corners=${ROUNDED};\
Maximum_Motor_Diameter=${MAXMOTOR};\
Minimum_Jacket_Thickness=${JACKET};\
Motor_Hole_Scaling=${SCALE};\
Resolution=${RESOLUTION};\
Which_Parts=\"frame\";\
"\
 Dream\ Bezier.scad &> /dev/null
done

