![Micro Cloud Quadcopter with Curtis Beef's board](https://bitbucket.org/repo/p5xnz4/images/1334630935-Beef.jpg)

![Micro Cloud (Concept) Hexacopter with MultiWii](https://bitbucket.org/repo/p5xnz4/images/2251526351-MultiWii-hexa_I.jpg)

![Quad Hexa and Octa Micro Cloud Copter frame stls](https://bitbucket.org/repo/p5xnz4/images/2272660558-Cloud_Copter_stls.png)

### Micro Cloud Copter Frame ###

* A customizable brushed micro multicopter frame inspired by Benedikt Haak's dream catcher quadcopter frame ([The Ultimate Micro Quad](http://www.thingiverse.com/thing:431290)).  It has a motor pop-off feature that protects the quad during impacts, and the floating Flight Deck which dampens vibrations.  Several valuable improvements are implemented:

1. Each motor is fixed to a casing which protects the leads, reduces motor twisting, and secures motor end-caps during crashes.

2. The Micro Cloud Copter can be customised to suit any reasonable number of motors, any multicopter configuration, and any size of brushed motors, including mixed sizes (theoretically).

3. The Micro Cloud Copter is implemented in OpenScad for easy customisability.

* v0.0beta

### How do I get the STLs? ###

* Download the project from [here](https://bitbucket.org/alejandroerickson/micro-cloud-copter/downloads) and find some stls in the /out/ directory

### How do I customise my own multirotor? ###

* Download the project from [here](https://bitbucket.org/alejandroerickson/micro-cloud-copter/downloads) and install OpenScad.  You can either open the file Dream\ Bezier.scad and customise the variables there, or print from the command line with the build scripts, build_frames.sh and build_jackets.sh.  Hopefully those will be self-explanatory.

### Contribute and Request ###

* Send a message or an email to Alejandro Erickson

### Bugs, Warnings, Notes, and Upcoming Features ###

* I don't know whether or not the behaviour of the scad file is different when changing variables in the command line.
* Disclaimer: You are using this software at your own risk.  The Micro Cloud Copter frames or some related part may do severe damage to something, including your multi-rotor.  The developers of the project are not responsible for any damage that might occur as a result of using this software, even through negligence.
* The hexa, octo, and tri copter frames are not flight tested.  I have printed the hexa frame and I find it a bit flimsy because the struts are sharply bent in my version.  I will adjust the defaults for this in a future version.  Also, I expect the octo frame will not support Hubsan props due to interference.  This will also be corrected in future.

Upcoming features:

* Make "double" Motor Jacket for Y-hexacopter and +-octocopter.

* Hard-code a variety of useful Flight Decks (and stop using Benedikt's).
  
* Implement Motor Jacket with 5mm holes that do not cover too much of the motor.  

* Add bevel to bottom-side of Motor Jacket so that frame needs less post-printing prep work.  

* Design a Flight Deck that makes easy battery changes and Flight Controller removal.

* Allow more customisation of the control points of the (Bezier) struts so that we can, (a), make an H-frame style quad for FPV and, (b), not have every strut come inward so far (imagine butterfly shaped quads!)

### Copyright and Attributions ###

 Attribution credits: Chad Kirby, WilliamAAdams (Thingiverse user), MicroMotorWarehouse (Thingiverse user)

 Copyright (c) 2015 Alejandro Erickson, released under and subject to
 the license: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
 http://creativecommons.org/licenses/by-sa/4.0/